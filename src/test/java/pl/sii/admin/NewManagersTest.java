package pl.sii.admin;
/*
 * Copyright (c) 2019.  Sii Poland
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import io.qameta.allure.Description;
import io.qameta.allure.Feature;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.parallel.Execution;
import org.junit.jupiter.api.parallel.ExecutionMode;
import org.junit.jupiter.params.provider.CsvFileSource;
import org.openqa.selenium.WebElement;
import pl.sii.base.BaseTest;
import org.junit.jupiter.params.*;

import static org.assertj.core.api.Assertions.*;


@DisplayName("New Managers")
@Execution(ExecutionMode.CONCURRENT)
public class NewManagersTest extends BaseTest {


    @Tag("admin")
    @Feature("Feature: Create and save new manages")
    @Severity(SeverityLevel.NORMAL)
    @DisplayName("New Managers")
    @Description("This TC verify if admin is able to create a new managers")
    @ParameterizedTest
    @CsvFileSource(resources = "/ManagersDataSet.csv", numLinesToSkip = 1, delimiter = ';')
    public void newManagersTest(String username, String password, String shortcut, String academictitle, String gender,
                                String firstname, String lastname, String email, String emailsender, String phone,
                                String fax, String language, String notes, String portals, String authorities,
                                String searchcontexts, String file) {
        assertThat(
                application.open()
                        .signIn()
                        .withEmail("XYZ")
                        .withPassword("XYZ")
                        .submit()
                        .checkIfEmailCodeIsRequired("XYZ")
                        .checkMenuElements()
                        .navigateToEditManagersPage()
                        .openAddManagerForm()
                        .setUserName(username)
                        .setUserPassword(password)
                        .setShortcut(shortcut)
                        .setAcademicTitle(academictitle)
                        .setGender(gender)
                        .setFirstName(firstname)
                        .setLastName(lastname)
                        .setManagerEmail(email)
                        .setEmailSender(emailsender)
                        .setPhone(phone)
                        .setFax(fax)
                        .setLanguage(language)
                        .setNotes(notes)
                        .setPortals(portals)
                        .setAuthorities(authorities)
                        .setSearchcontexts(searchcontexts)
                        .uploadFile(file)
        );
    }
}
