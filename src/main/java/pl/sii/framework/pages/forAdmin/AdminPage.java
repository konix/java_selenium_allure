package pl.sii.framework.pages.forAdmin;

import io.qameta.allure.Step;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.*;
import org.openqa.selenium.support.FindBy;
import pl.sii.framework.base.component.Page;
import pl.sii.framework.pages.forAdmin.Administration.ManagerTemplatePage;
import pl.sii.framework.pages.forAdmin.CMS.CMSPage;
import pl.sii.framework.pages.forAdmin.ProfileAdministration.ProfilesPage;
import pl.sii.framework.pages.forAdmin.TenderManagement.AllTendersPage;
import pl.sii.framework.pages.forAdmin.TenderManagement.TenderTemplatePage;
import pl.sii.framework.pages.forPortal.PortalPage;

import java.util.List;

import static org.assertj.core.api.Assertions.*;


@Slf4j
public class AdminPage extends Page {

    public AdminPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(xpath = "//input[@id='j_username']")
    WebElement tf_userName;

    @FindBy(xpath = "//input[@id='j_password']")
    WebElement tf_userPassword;

    @FindBy(xpath = "//button[@id='submit_login']")
    WebElement bt_submit;

    @FindBy(xpath = "//div[@class='alert alert-danger fade in fade-in-alert']")
    WebElement tm_alert;

    @FindBy(xpath = "//input[@id='username']")
    WebElement tf_emailUserName;

    @FindBy(xpath = "//input[@id='code']")
    WebElement tf_emailCode;

    @FindBy(xpath = "//header[contains(text(),'E-Mail Code')]")
    WebElement lb_emailCode;

    @FindBy(xpath = "//div[@class='navbar navbar-default']//li")
    List<WebElement> bar_menu;

    @FindBy(xpath = "//table[@id='dt_basic']//tbody/tr")
    List<WebElement> list_searchResultsRecords;

    @FindBy(xpath = "//*[@href='/admin/manager/list']")
    WebElement lb_administration;

    @FindBy(xpath = "//*[@href='/admin/apolloStatistic/index/dataSource']")
    WebElement lb_tenderManagement;

    @FindBy(xpath = "//li[2]//a[1]//span[1]")
    WebElement bt_tenderManagement;

    @FindBy(xpath = "//a[contains(text(),'Publish Tender')]")
    WebElement bt_publishTender;

    @FindBy(xpath = "//a[contains(text(),'List of all tenders')]")
    WebElement bt_allTenders;

    @FindBy(xpath = "//*[@href='/admin/tenderViewEdit/createTender/12']")
    WebElement bt_defaultTenderTemplate;

    @FindBy(xpath = "//input[@id='search-fld']")
    WebElement tf_customerSearchBar;


    //NAVIGATION:
    @FindBy(xpath = "//*[@href='/admin/manager/index']")
    WebElement lb_editManagers;

    @FindBy(xpath = "//div[@id='portal']")
    WebElement lb_portal;

    @FindBy(xpath = "//*[@href='/admin/cmsPage/index']")
    WebElement lb_cms;

    @FindBy(xpath = "//*[@href='/admin/searchProfile/index']")
    WebElement lb_profileAdministration;

    @FindBy(xpath = "//div[@class='login-info']//span//a")
    WebElement lb_userName;

    @FindBy(xpath = "//div[@id='logout']")
    WebElement lb_logout;

    @Step("User sets input {text} to customer search bar")
    public AdminPage searchCustomer(String text, String amount) {
        log.info("Set {} to customer search bar", text);
        tf_customerSearchBar.click();
        tf_customerSearchBar.clear();
        tf_customerSearchBar.sendKeys(text);
        tf_customerSearchBar.sendKeys(Keys.ENTER);
        checkElementsAreDisplayed(fluentWaitForElements(list_searchResultsRecords, 10, 1), Integer.parseInt(amount));
        return this;
    }


    @Step("Check if menu bar is shown")
    public AdminPage checkMenuElements() {
        checkElementsAreDisplayed(bar_menu, 5);
        return this;
    }

    @Step("Check title")
    public AdminPage checkTitle(String title) {
        customCheckTitle(title);
        return this;
    }

    @Step("Navigate to the Profile administration ")
    public ProfilesPage navigateToTheProfileAdministrationPage() {
        fluentWaitForElement(lb_profileAdministration, 10, 1).click();
        return pageFactory.create(ProfilesPage.class);
    }

    @Step("Navigate to Edit Managers Page")
    public ManagerTemplatePage navigateToEditManagersPage() {
        fluentWaitForElement(lb_administration, 10, 1).click();
        fluentWaitForElement(lb_editManagers, 10, 1).click();
        return pageFactory.create(ManagerTemplatePage.class);
    }

    @Step("Navigate to List of All Tenders Page")
    public AllTendersPage navigateToListOfAllTendersPage() {
        fluentWaitForElement(lb_tenderManagement, 10, 1).click();
        fluentWaitForElement(bt_tenderManagement, 10, 1).click();
        fluentWaitForElement(bt_allTenders, 10, 1).click();
        return pageFactory.create(AllTendersPage.class);
    }


    @Step("Navigate to Administration Page")
    public AdminPage navigateToAdministrationPage() {
        lb_administration.click();
        return this;
    }

    @Step("Navigate the Portal Page")
    public PortalPage navigateToPortalPage() {
        return pageFactory.create(PortalPage.class);
    }


    @Step("Navigate to Administration Page")
    public CMSPage navigateToCMSPage() {
        lb_cms.click();
        return pageFactory.create(CMSPage.class);
    }

    @Step("Navigate to Default Tender Form Page")
    public TenderTemplatePage navigateToDefaultTenderForm() {
        fluentWaitForElement(lb_tenderManagement, 30, 1).click();
        fluentWaitForElement(bt_tenderManagement, 30, 1).click();
        fluentWaitForElement(bt_publishTender, 30, 1).click();
        fluentWaitForElement(bt_defaultTenderTemplate, 30, 1).click();

        return pageFactory.create(TenderTemplatePage.class);
    }

    @Step("Logout")
    public AdminPage logout() {
        lb_logout.click();
        return this;
    }

    @Step("Check If Email Code Is Required")
    public AdminPage checkIfEmailCodeIsRequired(String email) {
        try {
            lb_emailCode.isDisplayed();
            log.info("Login require to provide Email code");
            log.info("Get Email code");
            String emailCode = getEmailCodes("FROM TEST ENVIRONMENT - Ariadne: Login E-Mail Code");
            fluentWaitForElement(tf_emailUserName, 10, 2).sendKeys(email);
            fluentWaitForElement(tf_emailCode, 10, 2).sendKeys(emailCode);
            fluentWaitForElement(bt_submit, 10, 1).click();

        } catch (NoSuchElementException e) {
            log.info("Login not require to provide Email code");
        }
        return this;
    }

    @Step("User clicks on Submit button")
    public AdminPage submit() {
        log.info("Submit login form");
        bt_submit.click();

        return pageFactory.create(AdminPage.class);
    }

    @Step("User check if alert message is displayed and contain proper text")
    public AdminPage isAlertMessageDisplayed(String alert_text) {
        try {
            log.info("Check if alert message displayed");
            webDriverWait.until(webDriver -> tm_alert.isDisplayed());
            String text_from_alert = tm_alert.getText();
            log.info("Get alert text:" + text_from_alert);
            log.info("Check if alert message contain proper text");
            assertThat(text_from_alert).contains(alert_text);
            return this;
        } catch (NotFoundException | StaleElementReferenceException | ElementNotVisibleException e) {
            return this;
        }
    }

    @Step("checkIfLoggedUserNameIsCorrect")
    public AdminPage checkIfLoggedUserNameIsCorrect(String username) {
        customCheckText(username, lb_userName);
        return this;
    }

}