package pl.sii.portal;
/*
 * Copyright (c) 2019.  Sii Poland
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import io.qameta.allure.*;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.parallel.Execution;
import org.junit.jupiter.api.parallel.ExecutionMode;
import pl.sii.base.BaseTest;

import static org.assertj.core.api.Assertions.assertThat;


@DisplayName("Portal Overview Tests")
@Execution(ExecutionMode.CONCURRENT)
public class PortalOverviewTest extends BaseTest {

    @Link("https://page/about/below/tc.html#gid=0&range=A11")
    @Test
    @Tag("portal")
    @Feature("Feature: Open the products & prices page")
    @Severity(SeverityLevel.CRITICAL)
    @DisplayName("Open the products & prices page")
    @Description("This TC verify if user is able to open the products & prices page")


    public void openProductAndPricePage() {
        assertThat(
                application.openPortal()
                        .loginToThePortal("test@mail.com", "XYZ")
                        .openProductsPage()
                        .checkTitle("Products: test.XYZ")


        );
    }


    @Link("https://page/about/below/tc.html#gid=0&range=A10")
    @Test
    @Tag("portal")
    @Feature("Feature: Open the trial page")
    @Severity(SeverityLevel.CRITICAL)
    @DisplayName("Open the trial page")
    @Description("This TC verify if user is able to open the trial page")


    public void openTrialPage() {
        assertThat(
                application.openPortal()
                        .openTrialPage()
                        .checkTitle("Register to test.XYZ")

        );
    }

    @Link("https://page/about/below/tc.html#gid=0&range=A11")
    @Test
    @Tag("portal")
    @Feature("Feature: Open a public visible search profile via the quick search and the business selector")
    @Severity(SeverityLevel.CRITICAL)
    @DisplayName("Open a public visible search profile via the quick search and the business selector")
    @Description("This TC verify if user is able to open a public visible search profile via the quick search and the business selector")


    public void searchTenderViaQuickSearch() {
        assertThat(
                application.openPortal()
                        .findRelevantTenders("London")
                        .verifyTendersResultList(9)


        );
    }

    @Link("https://page/about/below/tc.html#gid=0&range=A13")
    @Test
    @Tag("portal")
    @Feature("Feature: Open a tender from the search result")
    @Severity(SeverityLevel.CRITICAL)
    @DisplayName("Open a tender from the search result")
    @Description("This TC verify if user is able to open a tender from the search result")


    public void openTenderFromTheSearchResultList() {
        assertThat(
                application.openPortal()
                        .acceptCookies()
                        .findRelevantTenders("London")
                        .openTenderFromResultList()


        );
    }

    @Link("https://page/about/below/tc.html#gid=0&range=A14")
    @Test
    @Tag("portal")
    @Feature("Feature: Check that fulltext is not visible")
    @Severity(SeverityLevel.NORMAL)
    @DisplayName("Check that fulltext is not visible")
    @Description("This TC verify if fulltext is not visible for anonymous user")


    public void checkThatFullTextIsNotVisible() {
        assertThat(
                application.openPortal()
                        .acceptCookies()
                        .findRelevantTenders("London")
                        .openTenderFromResultList()
                        .verifyIfFullTextIsDisplayed()
        );
    }

    @Link("https://page/about/below/tc.html#gid=0&range=A16")
    @Test
    @Tag("portal")
    @Feature("Feature: Download the tender as PDF")
    @Severity(SeverityLevel.NORMAL)
    @DisplayName("Download the tender as PDF")
    @Description("This TC verify if user is able to download the tender as PDF")


    public void downloadTheTenderAsPDF() throws InterruptedException {
        assertThat(
                application.openPortal()
                        .acceptCookies()
                        .findRelevantTenders("London")
                        .openTenderFromResultList()
                        .downloadFile()
        );
    }

    @Link("https://page/about/below/tc.html#gid=0&range=A15")
    @Test
    @Tag("portal")
    @Feature("Feature: Click forward and open the forward link from the forward mail")
    @Severity(SeverityLevel.CRITICAL)
    @DisplayName("Click forward and open the forward link from the forward mail")
    @Description("This TC verify if user is able to click forward and open the forward link from the forward mail")


    public void forwardAndOpenTheForwardLinkFromTheForwardMail() throws InterruptedException {
        assertThat(
                application.openPortal()
                        .acceptCookies()
                        .findRelevantTenders("London")
                        .openTenderFromResultList()
                        .forwardMail()
        );
    }

    @Link("https://page/about/below/tc.html#gid=0&range=A18")
    @Test
    @Tag("portal")
    @Feature("Feature: Notes/Add to Favorite must not be possible without being logged in")
    @Severity(SeverityLevel.CRITICAL)
    @DisplayName("Notes/Add to Favorite must not be possible without being logged in")
    @Description("This TC check if Notes/Add to Favorite are unavailable for anonymous users")


    public void notesAndFavouriteShallBeAvailableOnlyForLoggedUsers() throws InterruptedException {
        assertThat(
                application.openPortal()
                        .acceptCookies()
                        .findRelevantTenders("London")
                        .openTenderFromResultList()
                        .notesAndFavouriteShallBeAvailableOnlyForLoggedUsers()
                        .loginToThePortal("test@mail.com", "XYZ")
                        .notesAndFavouriteShallBeAvailableOnlyForLoggedUsers()
        );
    }

    @Link("https://page/about/below/tc.html#gid=0&range=A22")
    @Test
    @Tag("portal")
    @Feature("Feature: Register as already existing customer, check for double registration mail")
    @Severity(SeverityLevel.CRITICAL)
    @DisplayName("Register as already existing customer, check for double registration mail")
    @Description("This TC verify if double registration mail will be sent after registration already existing customer")


    public void checkForDoubleRegistrationMail() throws InterruptedException {
        assertThat(
                application.openPortal()
                        .acceptCookies()
                        .openRegistrationForm()
                        .selectTitle("Mr")
                        .setFirstName("John")
                        .setLastName("Doe")
                        .setStreet("Ostrowskiego")
                        .setPostCode("35-614")
                        .setCity("Warsaw")
                        .setCompanyName("Altimi")
                        .selectCountry("Poland")
                        .setPhone("333222111")
                        .setMobile("222333444")
                        .setProfile("Con")
                        .setEmail("123@tender-service.de")
                        .setBusinessSector("IT")
                        .setRegions()
                        .acceptTermsAndConditions()
                        //.submitRegistrationForm()
                        .getDoubleRegistrationMail()


        );
    }

}
