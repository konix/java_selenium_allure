package pl.sii.framework.pages.forPortal;

import com.sun.nio.zipfs.ZipPath;
import io.qameta.allure.Step;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import pl.sii.framework.base.component.Page;

import java.io.IOException;
import java.nio.file.*;
import java.io.File;
import java.nio.file.Path;
import java.util.Comparator;
import java.util.List;
import java.util.UUID;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;


@Slf4j
public class TenderViewPortalPage extends Page {

    public TenderViewPortalPage(WebDriver driver) {
        super(driver);
    }

    // Search result page
    @FindBy(xpath = "//*[contains(@id,'tableRow_')]")
    List<WebElement> tendersResultList;

    @FindBy(xpath = "//ul[@id='first-nav']//a[@class='link3']")
    WebElement lb_home;

    @FindBy(xpath = "/html[1]/body[1]/div[9]/div[1]/div[2]/div[2]")
    WebElement fullTextOpenBar;
    //h3[contains(text(),'My free trial:')]
    @FindBy(xpath = "//h3[contains(text(),'My free trial:')]")
    WebElement fullTextBoxForAnonymousUser;

    @FindBy(xpath = "//span[contains(text(),'Download')]")
    WebElement bt_download;

    @FindBy(xpath = "//body/div[@id='wrap']/div[@id='primary']/div[@id='content']/div[@id='detail']/div[@class='detailRow']/div[@class='detailCellSimple']/div/ul[@class='detail-options hide-on-print']/li[@class='item-send']/a[1]")
    WebElement bt_forward;

    @FindBy(xpath = "//span[contains(text(),'Notes')]")
    WebElement bt_notes;

    @FindBy(xpath = "//span[contains(text(),'Add to Favou')]")
    WebElement bt_favourites;


    @FindBy(xpath = " //span[@id='noteReturnMessageNotLogged']")
    WebElement noteReturnMessageNotLogged;

    @FindBy(xpath = "/html[1]/body[1]/div[8]/div[3]/div[1]/div[1]/a[1]")
    WebElement lb_login;

    @FindBy(xpath = "//input[@id='login-username']")
    WebElement tf_login;

    @FindBy(xpath = "//input[@id='fake-login-pass']")
    WebElement tf_passwordBeforeClick;

    @FindBy(xpath = "//input[@id='login-pass']")
    WebElement tf_passwordAfterClick;

    @FindBy(xpath = "//input[@id='submit_login']")
    WebElement bt_submit;

    @FindBy(xpath = "//div[@id='portal']")
    WebElement lb_portal;

    @FindBy(xpath = "//input[@id='autocompleteBranches']")
    WebElement tf_tenderSearchBar;

    @FindBy(xpath = "//input[@id='s_submit']")
    WebElement tf_submitSearchBar;

    @FindBy(xpath = "//ul[@id='first-nav']//a[@class='link1']")
    WebElement bt_trialOrProducts;


    @Step("Download file.")
    public TenderViewPortalPage downloadFile() throws InterruptedException {

        log.info("Download file");
        Path rootPath = Paths.get(System.getProperty("user.dir") + File.separator + "externalFiles" + File.separator + "downloadFiles");
        fluentWaitForElement(bt_download, 10, 1).click();
        Thread.sleep(2000);
        log.info("Verify if files exists");
        assertThat(Files.exists(rootPath)).isTrue();

        try (Stream<Path> walk = Files.walk(rootPath)) {
            walk.sorted(Comparator.reverseOrder())
                    .map(Path::toFile)
                    .peek(System.out::println)
                    .forEach(File::delete);
        } catch (IOException e) {
            e.printStackTrace();
        }

        assertThat(Files.exists(rootPath)).isFalse();
        log.info("File deleted successfully");

        return this;
    }

    @Step("Check if Notes/Add to Favorite are unavailable for anonymous users")
    public TenderViewPortalPage notesAndFavouriteShallBeAvailableOnlyForLoggedUsers() throws InterruptedException {

        log.info("Check if Notes/Add to Favorite are unavailable for anonymous users");

        if (bt_trialOrProducts.getText().equals("TRIAL")) {
            log.info("Click on note");
            fluentWaitForElement(bt_notes, 10, 1).click();

            noteReturnMessageNotLogged.isDisplayed();
            log.info("Notes is displayed.");
            verifyNoelement(bt_favourites);
            log.info("Add to Favorite is not displayed");
        } else {
            bt_favourites.isDisplayed();
            log.info("'Add to Favorite' option is displayed");
            fluentWaitForElement(bt_notes, 10, 1).click();
        }


        return this;
    }

    @Step("User sets input  Login: {login} Password: {password}")
    public TenderViewPortalPage loginToThePortal(String login, String password) {
        log.info("Fill {} / {} credentials", login, password);
        fluentWaitForElement(tf_login, 10, 1).click();
        tf_login.sendKeys(login);
        fluentWaitForElement(tf_passwordBeforeClick, 10, 1).click();
        tf_passwordAfterClick.sendKeys(password);
        bt_submit.click();
        return this;
    }

    @Step("Forward mail.")
    public TenderViewPortalPage forwardMail() throws InterruptedException {

        log.info("Forward mail.");
        fluentWaitForElement(bt_forward, 10, 1);
        String mailto = bt_forward.getAttribute("href");
        log.info(mailto);
        assertThat(mailto.contentEquals("mailto:?subject=Dynamnc%20Purchasing%20System%20for%20the%20Provision%20of%20Minor%20Works&body=This%20tender%20from%20XYZ%20could%20be%20interesting%20for%20you.%20https%3A%2F%2Ftest.XYZ%2FtenderInfo%2FshowTender%2Fb5c6d142-a9fd-11e9-860f-002655ffd6c8"));
        driver.get(mailto.substring(182, 286).replace("%2F", "/").replace("%3A", ":"));
        return this;
    }


    @Step("Verify if Full Text is displayed for anonymous user")
    public TenderViewPortalPage verifyIfFullTextIsDisplayed() {
        log.info("Verify if Full Text is displayed for anonymous user");
        fluentWaitForElement(fullTextOpenBar, 10, 1).click();
        fullTextBoxForAnonymousUser.isDisplayed();
        return this;
    }


}

