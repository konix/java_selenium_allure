package pl.sii.framework.pages.forAdmin.TenderManagement;

import io.qameta.allure.Step;
import lombok.extern.slf4j.Slf4j;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import pl.sii.framework.base.component.Page;


@Slf4j
public class TenderTemplatePage extends Page {

    public TenderTemplatePage(WebDriver driver) {
        super(driver);
    }


    @FindBy(xpath = "//textarea[@name='title_en']")
    WebElement ta_title;

    @FindBy(xpath = "//select[@id='currentTenderLang']")
    WebElement dd_language;

    @FindBy(xpath = "//input[@id='publicationDateInput']")
    WebElement tf_publicationDate;

    @FindBy(xpath = "//input[@id='systemExpirationDateInput']")
    WebElement tf_systemExpirationDate;

    @FindBy(xpath = "//select[@id='natureOfContract']")
    WebElement dd_natureOfContract;

    @FindBy(xpath = "//select[@id='typeOfDocument']")
    WebElement dd_typeOfDocument;

    @FindBy(xpath = "//input[@id='0_realisationAddressNutsCode']")
    WebElement tf_realisationAddressNutsCode;

    @FindBy(xpath = "//select[@id='status']")
    WebElement dd_status;

    @FindBy(xpath = "//iframe[@class='cke_wysiwyg_frame cke_reset'][@aria-describedby='cke_372']")
    WebElement frame_ckeEditor;

    @FindBy(tagName = "body")
    WebElement body_ckeEditor;

    @Step("User sets  Full Text {text}")
    public TenderTemplatePage setFullText(String text) {
        log.info("Set Full Text {}", text);
        fluentWaitForElement(frame_ckeEditor, 10, 1);
        driver.switchTo().frame(frame_ckeEditor);
        body_ckeEditor.sendKeys(text);
        driver.switchTo().defaultContent();
        return this;
    }

    @Step("User sets title {title} to login input field")
    public TenderTemplatePage setTitle(String title) {
        log.info("Set title {}", title);
        ta_title.sendKeys(title);
        return this;
    }

    @Step("User sets Realisation Address Nuts Code {nuts} to login input field")
    public TenderTemplatePage setRealisationAddressNutsCode(String nuts) {
        log.info("Set Realisation Address Nuts Code {}", nuts);
        tf_realisationAddressNutsCode.sendKeys(nuts);
        return this;
    }

    @Step("Select proper Language")
    public TenderTemplatePage setLanguage(String option) {
        selectDropDownOptionDataDriven(option, dd_language);
        return this;
    }

    @Step("User sets Publication Date {publicationDate} to login input field")
    public TenderTemplatePage setPublicationDate(String publicationDate) {
        log.info("Set Publication Date  {}", publicationDate);
        setDate(publicationDate, tf_publicationDate);
        return this;
    }

    @Step("User sets System Expiration Date {title} to login input field")
    public TenderTemplatePage setSystemExpirationDate(String systemExpirationDate) {
        log.info("Set System Expiration {}", systemExpirationDate);
        setDate(systemExpirationDate, tf_systemExpirationDate);
        return this;
    }

    @Step("Select proper Type Of Document")
    public TenderTemplatePage setTypeOfDocument(String option) {
        selectDropDownOptionDataDriven(option, dd_typeOfDocument);
        return this;
    }

    @Step("Select proper Nature Of Contract")
    public TenderTemplatePage setNatureOfContract(String option) {
        selectDropDownOptionDataDriven(option, dd_natureOfContract);
        return this;
    }

    @Step("Select proper Status")
    public TenderTemplatePage setStatus(String option) {
        selectDropDownOptionDataDriven(option, dd_status);
        return this;
    }
}