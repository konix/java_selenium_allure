/*
 * Copyright (c) 2019.  Sii Poland
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.sii.framework.pages;

import io.qameta.allure.Step;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.*;
import org.openqa.selenium.support.FindBy;
import pl.sii.framework.base.component.Page;
import pl.sii.framework.pages.forAdmin.AdminPage;

import static org.assertj.core.api.Assertions.*;

@Slf4j
public class SignInPage extends Page {

    public SignInPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(xpath = "//input[@id='j_username']")
    WebElement tf_userName;

    @FindBy(xpath = "//input[@id='j_password']")
    WebElement tf_userPassword;

    @FindBy(xpath = "//button[@id='submit_login']")
    WebElement bt_submit;

    @FindBy(xpath = "//div[@class='alert alert-danger fade in fade-in-alert']")
    WebElement tm_alert;

    @Step("Check title")
    public SignInPage checkTitle(String title) {
        customCheckTitle(title);
        return this;
    }


    @Step("User set login {email} to login input field")
    public SignInPage withEmail(String email) {
        log.info("Set email {}", email);
        tf_userName.sendKeys(email);
        return this;
    }


    @Step("User sets password to password input field")
    public SignInPage withPassword(String password) {
        log.info("Set password {}", password);
        tf_userPassword.sendKeys(password);
        return this;
    }

    @Step("User clicks on Submit button")
    public AdminPage submit() {
        log.info("Submit login form");
        bt_submit.click();

        return pageFactory.create(AdminPage.class);
    }


    @Step("User check if alert message is displayed and contain proper text")
    public SignInPage isAlertMessageDisplayed(String alert_text) {
        try {
            log.info("Check if alert message displayed");
            webDriverWait.until(webDriver -> tm_alert.isDisplayed());
            String text_from_alert = tm_alert.getText();
            log.info("Get alert text", text_from_alert);
            log.info("Check if alert message contain proper text");
            assertThat(text_from_alert).contains(alert_text);
            return this;
        } catch (NotFoundException | StaleElementReferenceException | ElementNotVisibleException e) {
            return this;
        }
    }

}