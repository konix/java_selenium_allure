package pl.sii.framework.pages.forAdmin.TenderManagement;

import io.qameta.allure.Step;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import pl.sii.framework.base.component.Page;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;


@Slf4j
public class AllTendersPage extends Page {

    public AllTendersPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(xpath = "/html[1]/body[1]/div[3]/div[2]/form[3]/fieldset[1]/div[1]/div[2]/div[1]/div[1]")
    WebElement bar_filter;

    @FindBy(xpath = "//select[@id='managerEditedId']")
    WebElement dd_managers;

    @FindBy(xpath = "//input[@id='submitButton']")
    WebElement bt_search;

    @FindBy(xpath = "//table[@id='dt_basic']//thead//tr//th//label[@class='checkbox']//i")
    WebElement cb_allRecords;

    @FindBy(xpath = "//*[contains(@class,'tenderLink linkNotDecorated')]")
    List<WebElement> allTitles;

    @FindBy(xpath = "/html[1]/body[1]/div[3]/div[2]/div[2]/div[1]/div[1]/form[1]/table[1]/tbody[1]//td[3]")
    List<WebElement> allStatus;

    @FindBy(xpath = "//div[@id='main']//tr[1]//td[3]")
    WebElement firstStatus;

    //MASS UPDATE elements
    @FindBy(xpath = "//body[@class='desktop-detected smart-style-3']/div[@id='main']/div[@id='content']/div[@id='grid']/div[@id='grid_container']/div[@class='table-responsive']/form[@class='smart-form']/table[@id='dt_basic']/thead/tr/td/div[1]/a[1]")
    WebElement massUpdateToStatus;

    @FindBy(xpath = "//a[@id='mass-status-update-0']")
    WebElement massUpdateToStatusInactive;

    @FindBy(xpath = "//a[@id='mass-status-update-1']")
    WebElement massUpdateToStatusActive;

    @FindBy(xpath = "//a[@id='mass-status-update-3']")
    WebElement massUpdateToStatusDeleted;


    @Step("Filter tender results by manager {manager}")
    public AllTendersPage filterTendersByManager(String manager) {
        log.info("Open filter form");
        fluentWaitForElement(bar_filter, 10, 1).click();
        log.info("Filter tender results by manager {}", manager);
        selectDropDownOptionDataDriven(manager, dd_managers);
        log.info("Click od the 'Search' button");
        fluentWaitForElement(bt_search, 10, 1).click();
        fluentWaitForElements(allStatus, 10, 2);
        return this;
    }

    @Step("Mass update")
    public AllTendersPage massUpdate() throws InterruptedException {
        log.info("Get text from all Titles");
        Thread.sleep(2000);
        String[] statusesTabBefore = getTextFromEachElements(allStatus);
        fluentWaitForElement(cb_allRecords, 10, 2).click();
        fluentWaitForElement(massUpdateToStatus, 10, 2).click();
        log.info("massUpdateToStatusActive");
        String status = firstStatus.getText();
        if (status.equals("inactive")) {
            fluentWaitForElement(massUpdateToStatusActive, 10, 2).click();
        } else {
            fluentWaitForElement(massUpdateToStatusInactive, 10, 2).click();
        }
        confirmAlertMessage();
        Thread.sleep(2000);
        fluentWaitForElement(bt_search, 10, 2).click();
        fluentWaitForElements(allStatus, 10, 2);
        log.info("Get text from all Titles");
        String[] statusesTabAfter = getTextFromEachElements(allStatus);
        assertThat(statusesTabBefore).isNotEqualTo(statusesTabAfter);

        return this;
    }

}