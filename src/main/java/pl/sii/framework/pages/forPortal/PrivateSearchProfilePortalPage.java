package pl.sii.framework.pages.forPortal;

import io.qameta.allure.Step;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import pl.sii.framework.base.component.Page;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;


@Slf4j
public class PrivateSearchProfilePortalPage extends Page {

    public PrivateSearchProfilePortalPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(xpath = "//div[@class='publicMessage']")
    WebElement lb_publicMessage;

    @FindBy(xpath = "//a[@id='openSaveAsNewProfile']")
    WebElement bt_saveAs;

    @FindBy(xpath = "//input[@id='searchButton']")
    WebElement bt_search;

    //EDIT SEARCH PROFILE

    @FindBy(xpath = "//a[@id='edit-profilename']")
    WebElement bt_editSearchProfileName;

    @FindBy(xpath = "//input[@id='profileNameId']")
    WebElement tf_editSearchProfileName;

    @FindBy(xpath = "//input[@name='_action_updateName']")
    WebElement tf_editUpdateSearchProfileName;

    @FindBy(xpath = "//a[@id='openAddExcludedCPV']")
    WebElement lb_editExcludedCPV;

    @FindBy(xpath = "//textarea[@id='excludedCpvToAdd']")
    WebElement ta_excludedCpvToAdd;

    @FindBy(xpath = "//input[@name='_action_addExcludedCPV']")
    WebElement bt_addExcludedCPV;

    @FindBy(xpath = "//div[@class='red-tag']")
    WebElement lb_addedExcludedCPV;

    @FindBy(xpath = "//span[contains(text(),'Type of Document')]")
    WebElement lb_filterTypeOfDocument;

    @FindBy(xpath = "//li[3]//div[1]//label[5]//input[1]")
    WebElement cb_correctionTypeOfDocument;

    @FindBy(xpath = "/html[1]/body[1]/div[11]/div[1]/div[1]/form[1]/div[1]/div[1]/ul[1]/li[3]/div[1]/div[1]/input[1]")
    WebElement bt_confirmTypeOfDocumentEdition;

    @FindBy(xpath = "/html[1]/body[1]/div[11]/div[1]/div[1]/form[1]/div[1]/div[1]/ul[1]/li[3]/a[1]/span[2]")
    WebElement lb_filterTypeOfDocumentInfo;

    @FindBy(xpath = "/html[1]/body[1]/div[11]/div[1]/div[1]/form[1]/div[2]/div[2]/div[3]/div[1]/div[1]/div[1]/a[1]/span[1]")
    WebElement bt_saveEditedSearchProfile;

    //SAVE AS FORM

    @FindBy(xpath = "//a[@class='plus-link u']")
    WebElement bt_addContact;

    @FindBy(xpath = "//input[@id='newProfileName']")
    WebElement tf_searchProfileName;

    @FindBy(xpath = "//div//div//div//div//div//div//div//div//div//div//div//div[1]//select[1]")
    WebElement dd_genderOfFirstRecipient;

    @FindBy(xpath = "//div//div//div//div//div//div//div[3]//select[1]")
    WebElement dd_genderOfSecondRecipient;

    @FindBy(xpath = "/html[1]/body[1]/div[11]/div[1]/div[1]/form[1]/div[2]/div[2]/div[3]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/table[1]/tbody[1]/tr[1]/td[2]/div[1]/input[1]")
    WebElement tf_firstNameOfFirstRecipient;

    @FindBy(xpath = "//div//div//div//div//div//div//div//div//div//div//div//div//div[3]//input[1]")
    WebElement tf_firstNameOfSecondRecipient;

    @FindBy(xpath = "//div//div//div//div//div//div//div//div//div//div//div//div[1]//input[2]")
    WebElement tf_lastNameOfFirstRecipient;

    @FindBy(xpath = "//div//div//div//div//div//div//div[3]//input[2]")
    WebElement tf_lastNameOfSecondRecipient;

    @FindBy(xpath = "//div//div//div//div//div//div//div//div//div//div//div//div[1]//input[3]")
    WebElement tf_emailOfFirstRecipient;

    @FindBy(xpath = "//div//div//div//div//div//div//div[3]//input[3]")
    WebElement tf_emailOfSecondRecipient;

    @FindBy(xpath = "//input[@id='btnSaveAsCustomCSP']")
    WebElement bt_save;

    //MENU
    @FindBy(xpath = "//ul[@id='first-nav']/li")
    List<WebElement> allMenuOptions;

    @FindBy(xpath = "//ul[@id='first-nav']//a[@class='link3']")
    WebElement lb_home;

    @FindBy(xpath = "//ul[@id='first-nav']//a[@class='link1']")
    WebElement lb_trial;

    @FindBy(xpath = "//ul[@id='first-nav']//a[@class='link1'][contains(text(),'Products')]")
    WebElement lb_products;

    @FindBy(xpath = "//*[@href='/private/searchprofile/profiles?currentItem=profiles']")
    WebElement lb_myProducts;

    // Search result page

    @FindBy(xpath = "//*[contains(@id, 'found')]")
    WebElement lb_infoAboutTendersAmount;

    @FindBy(xpath = "//body/div/div/div/form/div/div/div/div/table/tbody/tr[1]/td[1]/a[1]")
    WebElement lb_searchProfileName;


    @Step("Open save search-profile form")
    public PrivateSearchProfilePortalPage openSaveSearchProfileForm() {
        log.info("Click on the Save AS button.");
        fluentWaitForElement(bt_saveAs, 10, 1).click();
        return pageFactory.create(PrivateSearchProfilePortalPage.class);
    }

    @Step("Add contact in the search-profile form")
    public PrivateSearchProfilePortalPage addContact() {
        log.info("Click on the add contact button.");
        fluentWaitForElement(bt_addContact, 10, 1).click();
        return this;
    }

    @Step("Click on the 'Search' button")
    public PrivateSearchProfilePortalPage performSearch() {
        log.info("Click on the search button.");
        fluentWaitForElement(bt_search, 10, 1).click();
        return this;
    }

    @Step("Verify search results")
    public PrivateSearchProfilePortalPage editAndVerifySearchResult(String excludedCPV, String searchProfileName) {
        log.info("Verify search results");
        performSearch();
        String resultMessageBefore = lb_infoAboutTendersAmount.getText();
        editProfileName(searchProfileName);
        editExcludedCPV(excludedCPV);
        editTypeOfDocument();
        performSearch();
        String resultMessageAfter = lb_infoAboutTendersAmount.getText();
        String tendersAmountFromResultMessage = lb_infoAboutTendersAmount.getText().substring(0, lb_infoAboutTendersAmount.getText().indexOf(" tenders"));
        Page.tendersAmount = Integer.parseInt(tendersAmountFromResultMessage);
        assertThat(resultMessageBefore).isNotEqualTo(resultMessageAfter);
        return this;
    }

    @Step("Save edited search profile")
    public PrivateSearchProfilePortalPage saveEditedSearchProfile() {
        log.info("Save edited search profile");
        bt_saveEditedSearchProfile.click();
        return this;
    }

    @Step("Set search profile name {profileName}")
    public PrivateSearchProfilePortalPage setProfileName(String profileName) {
        log.info("Set search profile name {}", profileName);
        tf_searchProfileName.clear();
        tf_searchProfileName.sendKeys(profileName);
        return this;
    }

    @Step("Fill first email recipient")
    public PrivateSearchProfilePortalPage setFirstContactData(String gender, String firstName, String lastName, String email) {
        log.info("Fill first name");
        selectDropDownOptionDataDriven("FEMALE", dd_genderOfFirstRecipient);
        tf_firstNameOfFirstRecipient.clear();
        tf_lastNameOfFirstRecipient.clear();
        tf_emailOfFirstRecipient.clear();
        tf_firstNameOfFirstRecipient.sendKeys(firstName);
        tf_lastNameOfFirstRecipient.sendKeys(lastName);
        tf_emailOfFirstRecipient.sendKeys(email);
        return this;
    }

    @Step("Fill second email recipient")
    public PrivateSearchProfilePortalPage setSecondContactData(String gender, String firstName, String lastName, String email) {
        selectDropDownOptionDataDriven("MALE", dd_genderOfFirstRecipient);
        tf_firstNameOfSecondRecipient.clear();
        tf_lastNameOfSecondRecipient.clear();
        tf_emailOfSecondRecipient.clear();
        tf_firstNameOfSecondRecipient.sendKeys(firstName);
        tf_lastNameOfSecondRecipient.sendKeys(lastName);
        tf_emailOfSecondRecipient.sendKeys(email);
        return this;
    }

    @Step("Save new search profile")
    public PrivateSearchProfilePortalPage submitForm() {
        bt_save.click();
        return this;
    }

    @Step("Verify if new SP has been successfully created.")
    public PrivateSearchProfilePortalPage verifyIfSPHasBeenCreated() {
        log.info("Verify if new SP has been successfully created");
        lb_publicMessage.isDisplayed();
        customCheckText("Search profile saved successfully!", lb_publicMessage);

        return this;
    }

    @Step("Verify if new SP has been successfully edited.")
    public PrivateSearchProfilePortalPage verifyIfSPHasBeenEdited() {
        log.info("Verify if new SP has been successfully edited.");
        lb_publicMessage.isDisplayed();
        customCheckText("Search profile saved or updated", lb_publicMessage);

        return this;
    }

    @Step("Navigate to the My Products Page ")
    public MyProductsPortalPage navigateToTheMyProductsPage() {
        log.info("Navigate to the My Products Page.");
        lb_myProducts.click();
        return pageFactory.create(MyProductsPortalPage.class);
    }

    //SP EDITING methods


    @Step("Edit search profile name")
    public PrivateSearchProfilePortalPage editProfileName(String searchProfileName) {
        log.info("Edit search profile name");
        bt_editSearchProfileName.click();
        fluentWaitForElement(tf_editSearchProfileName, 10, 1).clear();
        tf_editSearchProfileName.sendKeys(searchProfileName);
        tf_editUpdateSearchProfileName.click();
        fluentWaitForElement(lb_searchProfileName, 10, 1);
        customCheckText(searchProfileName, lb_searchProfileName);

        return this;
    }

    @Step("Edit excluded CPV")
    public PrivateSearchProfilePortalPage editExcludedCPV(String excludedCPV) {
        log.info("Edit search profile name");
        lb_editExcludedCPV.click();
        ta_excludedCpvToAdd.sendKeys(excludedCPV);
        bt_addExcludedCPV.click();
        customCheckText(excludedCPV, lb_addedExcludedCPV);

        return this;
    }

    @Step("Edit Type of Document")
    public PrivateSearchProfilePortalPage editTypeOfDocument() {
        log.info("Edit Type of Document");
        lb_filterTypeOfDocument.click();
        cb_correctionTypeOfDocument.click();
        bt_confirmTypeOfDocumentEdition.click();
        customCheckText("(Correction)", lb_filterTypeOfDocumentInfo);
        return this;
    }


}

