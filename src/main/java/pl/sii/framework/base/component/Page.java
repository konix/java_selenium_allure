/*
 * Copyright (c) 2019.  Sii Poland
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.sii.framework.base.component;

import io.qameta.allure.Step;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ArrayUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.Alert;
import pl.sii.framework.base.factory.PageFactory;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.Wait;

import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import javax.mail.*;
import java.io.IOException;
import java.util.*;
import java.util.Arrays;
import javax.mail.internet.MimeMultipart;

import static org.assertj.core.api.Assertions.*;

import java.util.List;
import java.util.stream.Collectors;

import org.openqa.selenium.JavascriptExecutor;
import pl.sii.framework.pages.forAdmin.AdminPage;
import pl.sii.framework.pages.forPortal.PortalPage;


@Slf4j
public abstract class Page {
    public static int tendersAmount;

    @Getter
    protected WebDriver driver;
    @Getter
    protected WebDriverWait webDriverWait;
    protected PageFactory pageFactory;

    public Page(WebDriver driver) {
        this.driver = driver;
        this.webDriverWait = new WebDriverWait(driver, 15);
        pageFactory = new PageFactory(driver);
    }

    public String getEmailCodes(String mailTitle) {
        try {
            Properties properties = System.getProperties();
            Session session = Session.getDefaultInstance(properties);

            Store store = session.getStore("imaps");
            store.connect("imap.XYZ", 993, "testmails@XYZ", "xyz");

            Folder folder = store.getFolder("inbox");

            if (!folder.exists()) {
                System.out.println("No INBOX...");
                System.exit(0);
            }
            folder.open(Folder.READ_WRITE);
            Message[] msg = folder.getMessages();
            String result = "";
            for (int i = 0; i < msg.length; i++) {

                if (msg[i].getSubject().equals(mailTitle)) {
                    String contentType = msg[i].getContentType();
                    if (contentType.contains("multipart/mixed") && i == (msg.length - 1)) {
                        MimeMultipart mimeMultipart = (MimeMultipart) msg[i].getContent();
                        result = getTextFromMimeMultipart(mimeMultipart);
                        log.info("Ariadne: Login E-Mail Code: " + result);
                    }
                }
            }

            store.close();
            return result;
        } catch (MessagingException | IOException e) {
            return e.getMessage();
        }
    }

    private String getTextFromMimeMultipart(
            MimeMultipart mimeMultipart) {
        try {
            String result = "";
            int count = mimeMultipart.getCount();
            for (int i = 0; i < count; i++) {
                BodyPart bodyPart = mimeMultipart.getBodyPart(i);
                if (bodyPart.isMimeType("text/plain")) {
                    result = result + bodyPart.getContent();
                    break; // without break same text appears twice in my tests
                } else if (bodyPart.isMimeType("text/html")) {
                    String html = (String) bodyPart.getContent();
                    result = result + org.jsoup.Jsoup.parse(html).text();
                } else if (bodyPart.getContent() instanceof MimeMultipart) {
                    result = result + getTextFromMimeMultipart((MimeMultipart) bodyPart.getContent());
                }
            }
            return result;
        } catch (MessagingException | IOException e) {
            return e.getMessage();
        }

    }

    public List<Element> findElements(final Locator locator) {
        return driver.findElements(locator.by())
                .stream()
                .map(Element::new)
                .collect(Collectors.toList());
    }

    public Element findElement(final Locator locator) {
        return new Element(driver.findElement(locator.by()));
    }


    public void refresh() {
        driver.navigate().refresh();
    }

    public boolean isDisplayed(final Locator locator) {
        try {
            return driver.findElement(locator.by()).isDisplayed();
        } catch (NotFoundException | StaleElementReferenceException | ElementNotVisibleException e) {
            return false;
        }
    }

    public static boolean isDisplayed(WebElement element) {
        try {
            return element.isDisplayed();
        } catch (NotFoundException | StaleElementReferenceException | ElementNotVisibleException e) {
            return false;
        }
    }

    public boolean verifyNoelement(WebElement elem) {
        try {
            if (elem.isDisplayed()) {
                return false;
            }
            return false;
        } catch (Exception e) {
            log.info("No element");
            return true;
        }


    }

    public Page confirmAlertMessage() {
        try {
            log.info("Wait until alertIsPresent");
            webDriverWait.until(webDriver -> ExpectedConditions.alertIsPresent());
            log.info("Switching to Alert");
            Alert alert = driver.switchTo().alert();
            log.info("Accepting alert");
            alert.accept();
            driver.switchTo().defaultContent();
            return this;
        } catch (NoAlertPresentException e) {
            return this;
        }
    }

    @Step("Count elements and get text from each")
    public String[] getTextFromEachElements(List<WebElement> locator) {
        fluentWaitForElements(locator, 10, 2);
        log.info("Check if elements are displayed");
        System.out.println("Number of elements: " + locator.size());
        String[] texts = new String[0];

        for (WebElement webElement : locator) {
            String text = webElement.getText();
            // System.out.println("Text from element: " + webElement.getText());
            texts = ArrayUtils.add(texts, text);
        }

        for (String s : texts) {
            log.info(s);
        }

        return texts;
    }

    @Step("Count elements and get text from each")
    public String[] getTextFromElement(WebElement locator) {
        fluentWaitForElement(locator, 10, 2);
        log.info("Check if element is displayed");
        String[] texts = new String[0];
        String text = locator.getText();
        // System.out.println("Text from element: " + webElement.getText());
        texts = ArrayUtils.add(texts, text);


        for (String s : texts) {
            log.info(s);
        }

        return texts;
    }

    @Step("Check if elements are displayed")
    public Page checkElementsAreDisplayed(List<WebElement> locator, int elementsExpectedSize) {
        log.info("Check if elements are displayed");
        System.out.println("Number of elements: " + locator.size());

        if (locator.size() == elementsExpectedSize) {
            for (WebElement webElement : locator) {
                //System.out.println("Radio button text:" + webElement.getAttribute("href"));
                //System.out.println("Text from element: " + webElement.getText());
            }
        } else assertThat(locator.size()).isEqualTo(elementsExpectedSize);
        return this;
    }

    public boolean selectDropDownOptionDataDriven(String option, WebElement elem) {
        try {
            log.info("Set option {}", option);
            Select mySelect = new Select(elem);
            mySelect.selectByVisibleText(option);
            return true;
        } catch (NotFoundException | StaleElementReferenceException | ElementNotVisibleException e) {
            return false;
        }
    }

    public boolean selectMultipleElementsDataDriven(String option, WebElement elem) {
        try {
            String[] options;
            options = option.split("\\$");
            for (String s : options) {
                log.info("Set option {}", s);
                Select mySelect = new Select(elem);
                mySelect.selectByVisibleText(s);
            }
            return true;
        } catch (NotFoundException | StaleElementReferenceException | ElementNotVisibleException e) {
            return false;
        }
    }

    public boolean checkIfElementIstNotInteractable(WebElement elem) {
        try {
            elem.isDisplayed();
            return false;
        } catch (Exception ex) {
            ex.printStackTrace();
            return true;
        }
    }

    public List<WebElement> fluentWaitForElements(List<WebElement> elementList, int timoutSec, int pollingSec) {

        Wait<WebDriver> wait = new FluentWait<WebDriver>(driver).withTimeout(Duration.ofSeconds(timoutSec))
                .pollingEvery(Duration.ofSeconds(pollingSec))
                .ignoring(NoSuchElementException.class)
                .ignoring(TimeoutException.class)
                .ignoring(StaleElementReferenceException.class)
                .ignoring(ElementClickInterceptedException.class);

        try {
            wait.until(ExpectedConditions.visibilityOfAllElements(elementList));
        } catch (Exception e) {

            System.out.println("Element Not found" + elementList.toString().substring(70));
            e.printStackTrace();

        }

        return elementList;

    }

    public WebElement waitForInvisableElement(WebElement element, int timoutSec, int pollingSec) {

        Wait<WebDriver> wait = new FluentWait<WebDriver>(driver).withTimeout(Duration.ofSeconds(timoutSec))
                .pollingEvery(Duration.ofSeconds(pollingSec))
                .ignoring(NoSuchElementException.class)
                .ignoring(TimeoutException.class)
                .ignoring(StaleElementReferenceException.class)
                .ignoring(ElementClickInterceptedException.class);

        try {
            //fWait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//*[@id='reportmanager-wrapper']/div[1]/div[2]/ul/li/span[3]/i[@data-original--title='We are processing through trillions of data events, this insight may take more than 15 minutes to complete.']")));
            wait.until(ExpectedConditions.invisibilityOf(element));
            //  wait.until(ExpectedConditions.elementToBeClickable(element));
        } catch (Exception e) {

            System.out.println("Element Not found" + element.toString().substring(70));
            e.printStackTrace();

        }

        return element;

    }


    public WebElement fluentWaitForElement(WebElement element, int timoutSec, int pollingSec) {

        Wait<WebDriver> wait = new FluentWait<WebDriver>(driver).withTimeout(Duration.ofSeconds(timoutSec))
                .pollingEvery(Duration.ofSeconds(pollingSec))
                .ignoring(NoSuchElementException.class)
                .ignoring(TimeoutException.class)
                .ignoring(StaleElementReferenceException.class)
                .ignoring(ElementClickInterceptedException.class);

        try {
            //fWait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//*[@id='reportmanager-wrapper']/div[1]/div[2]/ul/li/span[3]/i[@data-original--title='We are processing through trillions of data events, this insight may take more than 15 minutes to complete.']")));
            wait.until(ExpectedConditions.visibilityOf(element));
            wait.until(ExpectedConditions.elementToBeClickable(element));
        } catch (Exception e) {

            System.out.println("Element Not found" + element.toString().substring(70));
            e.printStackTrace();

        }

        return element;

    }

//    @Step("Open Portal")
//    public String openPortal(WebElement elem) {
//        try {
//            String winHandleBefore = driver.getWindowHandle();
//            fluentWaitForElement(elem,10,1).click();
//            for(String winHandle : driver.getWindowHandles()){
//                driver.switchTo().window(winHandle);
//            }
//            return winHandleBefore;
//        } catch (NotFoundException | StaleElementReferenceException | ElementNotVisibleException e) {
//            return e.getMessage();
//        }}
//
//    @Step("Close Portal")
//    public boolean closePortal(String winHandleBefore) {
//        try {
//        driver.close();
//        driver.switchTo().window(winHandleBefore);
//        return true;
//    } catch (NotFoundException | StaleElementReferenceException | ElementNotVisibleException e) {
//        return false;
//    }}


    public boolean setDate(String option, WebElement elem) {
        try {
            fluentWaitForElement(elem, 10, 1);
            JavascriptExecutor js = (JavascriptExecutor) driver;
            js.executeScript("arguments[0].value=arguments[1]", elem, option);
            return true;
        } catch (NotFoundException | StaleElementReferenceException | ElementNotVisibleException e) {
            return false;
        }
    }


    public boolean customCheckText(String textFromTest, WebElement element) {
        try {
            log.info("Get text from webelement: " + element);
            String textFromPage = element.getText();
            log.info("Text from test data set: " + textFromTest);
            log.info("Compare if text from test contains text from page: ");
            assertThat(textFromTest).contains(textFromPage);
            return true;
        } catch (NotFoundException | StaleElementReferenceException | ElementNotVisibleException e) {
            return false;
        }
    }

    public boolean customCheckTitle(String title) {
        try {
            String title_from_page = driver.getTitle();
            log.info("Title from page: " + title_from_page);
            log.info("Title from test data set: " + title);
            assertThat(title_from_page).contains(title);
            return true;
        } catch (NotFoundException | StaleElementReferenceException | ElementNotVisibleException e) {
            return false;
        }
    }
}
