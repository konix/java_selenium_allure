package pl.sii.portal;
/*
 * Copyright (c) 2019.  Sii Poland
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import io.qameta.allure.*;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.parallel.Execution;
import org.junit.jupiter.api.parallel.ExecutionMode;
import pl.sii.base.BaseTest;

import static org.assertj.core.api.Assertions.assertThat;


@DisplayName("Customer Login")
@Execution(ExecutionMode.CONCURRENT)
public class LoginToThePortalTest extends BaseTest {

    @Link("https://page/about/below/tc.html#gid=0&range=A7")
    @Link("https://page/about/below/tc.html#gid=0&range=A21")
    @Test
    @Tag("portal")
    @Feature("Feature: Customer Login")
    @Severity(SeverityLevel.CRITICAL)
    @DisplayName("Customer Login")
    @Description("This TC verify if Customer is able to login to the portal from admin panel")


    public void portalChecking() {
        assertThat(
                application.open()
                        .signIn()
                        .withEmail("XYZ")
                        .withPassword("XYZ123")
                        .submit()
                        .checkIfEmailCodeIsRequired("XYZ")
                        .checkMenuElements()
                        .navigateToPortalPage()
                        .openPortal()
                        .checkTitle("Public tenders from the UK, Ireland and Europe - XYZ")
                        .loginToThePortal("test@mail.com", "XYZ")
                        .checkTitle("XYZ")
        );
    }
}
