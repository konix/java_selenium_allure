package pl.sii.admin;
/*
 * Copyright (c) 2019.  Sii Poland
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import io.qameta.allure.*;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.parallel.Execution;
import org.junit.jupiter.api.parallel.ExecutionMode;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;
import pl.sii.base.BaseTest;

import static org.assertj.core.api.Assertions.assertThat;


@DisplayName("Logged in manager: Search for customers")
@Execution(ExecutionMode.CONCURRENT)
public class SearchForCustomersTest extends BaseTest {

    @Tag("admin")
    @Link("https://page/about/below/tc.html#gid=0&range=A28")
    @Feature("Feature: Search for customer search profiles/region search profiles/branch search profiles")
    @Severity(SeverityLevel.NORMAL)
    @DisplayName("Logged in manager: Search for customer ")
    @Description("This TC verify if Manager is able to search for customer")
    @ParameterizedTest
    @CsvFileSource(resources = "/CustomerSearchDataSet.csv", numLinesToSkip = 1, delimiter = ';')

    public void searchForCustomersTest(String text, String amount) {
        assertThat(
                application.open()
                        .signIn()
                        .withEmail("XYZ")
                        .withPassword("XYZ123")
                        .submit()
                        .checkIfEmailCodeIsRequired("XYZ")
                        .checkMenuElements()
                        .searchCustomer(text, amount)
        );
    }
}
