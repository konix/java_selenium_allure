package pl.sii.framework.pages.forPortal;

import io.qameta.allure.Step;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import pl.sii.framework.base.component.Page;

import java.util.List;


@Slf4j
public class IndustryProfilesPortalPage extends Page {

    public IndustryProfilesPortalPage(WebDriver driver) {
        super(driver);
    }

    //Custom Search

    @FindBy(xpath = "//*[@href='/private/custom/searchprofile/index']")
    WebElement lb_customSearch;


    //MENU
    @FindBy(xpath = "//ul[@id='first-nav']/li")
    List<WebElement> allMenuOptions;

    @FindBy(xpath = "//ul[@id='first-nav']//a[@class='link3']")
    WebElement lb_home;

    @FindBy(xpath = "//ul[@id='first-nav']//a[@class='link1']")
    WebElement lb_trial;

    @FindBy(xpath = "//ul[@id='first-nav']//a[@class='link1'][contains(text(),'Products')]")
    WebElement lb_products;


    @FindBy(xpath = "//*[@id='cookies']")
    WebElement bt_acceptCookies;


    // Search result page

    @Step("Open Custom Search Profiles Page.")
    public PrivateSearchProfilePortalPage openCustomSearchProfile() {
        log.info("Click on the Industry Profiles label");
        fluentWaitForElement(lb_customSearch, 10, 1).click();
        return pageFactory.create(PrivateSearchProfilePortalPage.class);
    }


}

