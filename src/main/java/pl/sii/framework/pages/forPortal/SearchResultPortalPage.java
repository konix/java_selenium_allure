package pl.sii.framework.pages.forPortal;

import io.qameta.allure.Step;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.*;
import org.openqa.selenium.support.FindBy;
import pl.sii.framework.base.component.Page;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;


@Slf4j
public class SearchResultPortalPage extends Page {

    public SearchResultPortalPage(WebDriver driver) {
        super(driver);
    }

    // Search result page
    @FindBy(xpath = "//*[contains(@id,'tableRow_')]")
    List<WebElement> tendersResultList;

    @FindBy(xpath = "//ul[@id='first-nav']//a[@class='link3']")
    WebElement lb_home;

    @FindBy(xpath = "/html[1]/body[1]/div[9]/div[2]/table[1]/tbody[1]/tr[1]/td[2]/a[1]")
    WebElement firstTenderFromResultList;

    @FindBy(xpath = "//*[contains(@id, 'found')]")
    WebElement lb_infoAboutTendersAmount;


    @Step("Verify if elements are displayed and they size is equal to {elementsAmount}")
    public SearchResultPortalPage verifyTendersResultList(int elementsAmount) {
        log.info("Verify if elements are displayed and they size is equal to {}", elementsAmount);
        checkElementsAreDisplayed(tendersResultList, elementsAmount);
        String resultMessageAfter = lb_infoAboutTendersAmount.getText().substring(0, lb_infoAboutTendersAmount.getText().indexOf(" tenders"));
        System.out.println(resultMessageAfter);
        return this;
    }


    @Step("Verify if elements are displayed and they size is equal to size form private arena")
    public SearchResultPortalPage verifyTendersResultListFromGlobalVar() {

        String resultMessageAfter = lb_infoAboutTendersAmount.getText().substring(0, lb_infoAboutTendersAmount.getText().indexOf(" tenders"));
        log.info("Verify if elements are displayed and they size is equal to {}", resultMessageAfter);
        System.out.println(resultMessageAfter);
        int tendersAmount = Integer.parseInt(resultMessageAfter);
        assertThat(tendersAmount).isEqualTo(Page.tendersAmount);
        return this;
    }

    @Step("Open tender from the result list")
    public TenderViewPortalPage openTenderFromResultList() {
        log.info("Open tender first tender from the result list");
        fluentWaitForElement(firstTenderFromResultList, 10, 1).click();
        return pageFactory.create(TenderViewPortalPage.class);
    }


}

