/*
 * Copyright (c) 2019.  Sii Poland
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.sii.framework.pages;

import io.qameta.allure.Step;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import pl.sii.framework.base.component.Page;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@Slf4j
public class MainPage extends Page {


    @FindBy(xpath = "//input[@id='j_username']")
    WebElement tf_userName;

    @FindBy(xpath = "//input[@id='j_password']")
    WebElement tf_userPassword;

    @FindBy(xpath = "//button[@id='submit_login']")
    WebElement bt_submit;

    @FindBy(xpath = "//div[@class='alert alert-danger fade in fade-in-alert']")
    WebElement tm_alert;

    @FindBy(xpath = "//input[@id='username']")
    WebElement tf_emailUserName;

    @FindBy(xpath = "//input[@id='code']")
    WebElement tf_emailCode;

    @FindBy(xpath = "//header[contains(text(),'E-Mail Code')]")
    WebElement lb_emailCode;

    @FindBy(xpath = "//div[@class='navbar navbar-default']//li")
    List<WebElement> bar_menu;

    public MainPage(WebDriver driver) {
        super(driver);
    }

    @Step("User navigate to 'Sign in' page")
    public SignInPage signIn() {
        log.info("Go to 'Sign in' page");
        return pageFactory.create(SignInPage.class);
    }

    @Step("Check title")
    public MainPage checkTitle(String title) {
        customCheckTitle(title);
        return this;
    }


}