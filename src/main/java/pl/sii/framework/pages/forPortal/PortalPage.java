package pl.sii.framework.pages.forPortal;

import io.qameta.allure.Step;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.*;
import org.openqa.selenium.support.FindBy;
import pl.sii.framework.base.component.Page;
import pl.sii.framework.pages.forAdmin.AdminPage;

import javax.mail.internet.MimeMultipart;

import java.io.File;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;


@Slf4j
public class PortalPage extends Page {

    public PortalPage(WebDriver driver) {
        super(driver);
    }

    //Home Page

    @FindBy(xpath = "/html[1]/body[1]/div[8]/div[3]/div[1]/div[1]/a[1]")
    WebElement lb_login;

    @FindBy(xpath = "//input[@id='login-username']")
    WebElement tf_login;

    @FindBy(xpath = "//input[@id='fake-login-pass']")
    WebElement tf_passwordBeforeClick;

    @FindBy(xpath = "//input[@id='login-pass']")
    WebElement tf_passwordAfterClick;

    @FindBy(xpath = "//input[@id='submit_login']")
    WebElement bt_submit;

    @FindBy(xpath = "//div[@id='portal']")
    WebElement lb_portal;

    @FindBy(xpath = "//input[@id='autocompleteBranches']")
    WebElement tf_tenderSearchBar;

    @FindBy(xpath = "//input[@id='s_submit']")
    WebElement tf_submitSearchBar;

    @FindBy(xpath = "//*[@href='/private/searchprofile/standardProfiles?currentItem=search']")
    WebElement lb_industryProfiles;


    //MENU
    @FindBy(xpath = "//ul[@id='first-nav']/li")
    List<WebElement> allMenuOptions;

    @FindBy(xpath = "//ul[@id='first-nav']//a[@class='link3']")
    WebElement lb_home;

    @FindBy(xpath = "//ul[@id='first-nav']//a[@class='link1']")
    WebElement lb_trial;

    @FindBy(xpath = "//ul[@id='first-nav']//a[@class='link1'][contains(text(),'Products')]")
    WebElement lb_products;

    @FindBy(xpath = "//*[@id='cookies']")
    WebElement bt_acceptCookies;

    @FindBy(xpath = "//*[@href='/wizardRegistration/reg'][@class='u red']")
    WebElement lb_newUser;

    @FindBy(xpath = "//*[@href='/private/searchprofile/profiles?currentItem=profiles']")
    WebElement lb_myProducts;


    // Search result page

    @Step("Accept cookies.")
    public PortalPage acceptCookies() {
        log.info("Click on the accept cookies button");
        bt_acceptCookies.click();
        return this;
    }


    @Step("Click on the Trial button.")
    public PortalPage openTrialPage() {
        log.info("Click on the TRIAL label");
        lb_trial.click();
        return this;
    }

    @Step("Find relevant tenders.")
    public SearchResultPortalPage findRelevantTenders(String searchPhrase) {
        log.info("Click on the tender search bar");
        tf_tenderSearchBar.click();
        log.info("Type {} in tenders search bar", searchPhrase);
        tf_tenderSearchBar.sendKeys(searchPhrase);
        log.info("Submit your tender searching");
        tf_submitSearchBar.click();
        return pageFactory.create(SearchResultPortalPage.class);
    }

    @Step("Open registration page..")
    public RegistrationFormPortalPage openRegistrationForm() {
        log.info("Click on the new user label");
        fluentWaitForElement(lb_newUser, 10, 1).click();
        return pageFactory.create(RegistrationFormPortalPage.class);
    }

    @Step("Open Industry Profiles Page.")
    public IndustryProfilesPortalPage openIndustryProfilesPage() {
        log.info("Click on the Industry Profiles label");
        fluentWaitForElement(lb_industryProfiles, 10, 1).click();
        return pageFactory.create(IndustryProfilesPortalPage.class);
    }

    @Step("Navigate to the My Products Page ")
    public MyProductsPortalPage openMyProductsPage() {
        log.info("Navigate to the My Products Page.");
        lb_myProducts.click();
        return pageFactory.create(MyProductsPortalPage.class);
    }


    @Step("Click on the Trial button.")
    public PortalPage openProductsPage() {
        log.info("Click on the TRIAL label");
        lb_products.click();
        return this;
    }

    @Step("Click on the hOMEl button.")
    public PortalPage openHomePage() {
        log.info("Click on the TRIAL label");
        lb_home.click();
        return this;
    }


    @Step("User sets input  Login: {login} Password: {password}")
    public PortalPage loginToThePortal(String login, String password) {
        log.info("Fill {} / {} credentials", login, password);
        fluentWaitForElement(tf_login, 10, 1).click();
        tf_login.sendKeys(login);
        fluentWaitForElement(tf_passwordBeforeClick, 10, 1).click();
        tf_passwordAfterClick.sendKeys(password);
        bt_submit.click();
        return this;
    }

    @Step("openPortal")
    public PortalPage openPortal() {
        try {
            String winHandleBefore = driver.getWindowHandle();
            log.info("{}", winHandleBefore);
            fluentWaitForElement(lb_portal, 10, 1).click();
            for (String winHandle : driver.getWindowHandles()) {
                driver.switchTo().window(winHandle);
            }
            return this;
        } catch (NotFoundException | StaleElementReferenceException | ElementNotVisibleException e) {
            return this;
        }
    }

    @Step("closePortal")
    public PortalPage closePortal() {
        try {
            driver.close();
            String winHandleBefore = driver.getWindowHandle();
            driver.switchTo().window(winHandleBefore);


            return this;
        } catch (NotFoundException | StaleElementReferenceException | ElementNotVisibleException e) {
            return this;
        }
    }

    @Step("Check title")
    public PortalPage checkTitle(String title) {
        customCheckTitle(title);
        return this;
    }

}

