package pl.sii.framework.pages.forPortal;

import io.qameta.allure.Step;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.*;
import org.openqa.selenium.support.FindBy;
import pl.sii.framework.base.component.Page;
import pl.sii.framework.pages.forAdmin.TenderManagement.TenderTemplatePage;

import java.util.List;


@Slf4j
public class RegistrationFormPortalPage extends Page {

    public RegistrationFormPortalPage(WebDriver driver) {
        super(driver);
    }

    //Registration form

    @FindBy(xpath = "//select[@id='param_salutation']")
    WebElement dd_title;

    @FindBy(xpath = "//input[@id='param_first_name']")
    WebElement tf_firstName;

    @FindBy(xpath = "//input[@id='param_last_name']")
    WebElement tf_lastName;

    @FindBy(xpath = "//input[@id='param_company']")
    WebElement tf_companyName;

    @FindBy(xpath = "//input[@id='param_street']")
    WebElement tf_addressStreet;

    @FindBy(xpath = "//input[@id='param_post_code']")
    WebElement tf_addressPostCode;

    @FindBy(xpath = "//input[@id='param_city_or_town']")
    WebElement tf_addressCity;

    @FindBy(xpath = "//select[@id='param_country']")
    WebElement dd_addressCountry;

    @FindBy(xpath = "//input[@id='param_email']")
    WebElement tf_email;

    @FindBy(xpath = "//input[@id='param_phone']")
    WebElement tf_phone;

    @FindBy(xpath = "//input[@id='param_mobile']")
    WebElement tf_mobile;

    @FindBy(xpath = "//input[@id='param_vat']")
    WebElement tf_vat;

    @FindBy(xpath = "//input[@id='BSPautocomplete']")
    WebElement tf_profile;

    @FindBy(xpath = "//ul[@class='ui-autocomplete ui-menu ui-widget ui-widget-content ui-corner-all']//li")
    List<WebElement> list_profileOptionList;

    @FindBy(xpath = "//ul[@class='ui-autocomplete ui-menu ui-widget ui-widget-content ui-corner-all']//li[1]")
    WebElement list_firstOptionFromProfileList;

    @FindBy(xpath = "//input[@id='param_branch']")
    WebElement tf_businessSector;

    @FindBy(xpath = "//input[@id='param.region_6899']")
    WebElement cb_regionUnitedKingdom;

    @FindBy(xpath = "//input[@id='param.region_4542']")
    WebElement cb_regionEurope;

    @FindBy(xpath = "//input[@id='param.automatic.email.consent']")
    WebElement cb_receiveNewsAboutTenders;

    @FindBy(xpath = "//input[@id='tcBox']")
    WebElement cb_termsAndConditions;

    @FindBy(xpath = "//input[@name='_action_finish']")
    WebElement bt_submitForm;

    //MENU
    @FindBy(xpath = "//ul[@id='first-nav']/li")
    List<WebElement> allMenuOptions;

    @FindBy(xpath = "//ul[@id='first-nav']//a[@class='link3']")
    WebElement lb_home;

    @FindBy(xpath = "//ul[@id='first-nav']//a[@class='link1']")
    WebElement lb_trial;

    @FindBy(xpath = "//ul[@id='first-nav']//a[@class='link1'][contains(text(),'Products')]")
    WebElement lb_products;


    @FindBy(xpath = "//*[@id='cookies']")
    WebElement bt_acceptCookies;


    // Search result page

    @Step("Accept cookies.")
    public RegistrationFormPortalPage getDoubleRegistrationMail() {
        log.info("Click on the accept cookies button");
        String emailCode = getEmailCodes("FROM TEST ENVIRONMENT - Double Registration attempt");
        log.info("FROM TEST ENVIRONMENT - Double Registration attempt (mail body): {}", emailCode);
        return this;
    }

    @Step("Select proper option from 'Title' drop down.")
    public RegistrationFormPortalPage selectTitle(String option) {
        selectDropDownOptionDataDriven(option, dd_title);
        return this;
    }

    @Step("User sets First Name {firstName} to proper text field.")
    public RegistrationFormPortalPage setFirstName(String firstName) {
        log.info("Set First Name {}", firstName);
        tf_firstName.sendKeys(firstName);
        return this;
    }

    @Step("User sets First Name {lastName} to proper text field.")
    public RegistrationFormPortalPage setLastName(String lastName) {
        log.info("Set Last Name {}", lastName);
        tf_lastName.sendKeys(lastName);
        return this;
    }

    @Step("User sets Company {company} to proper text field.")
    public RegistrationFormPortalPage setCompanyName(String company) {
        log.info("Set Company {}", company);
        tf_companyName.sendKeys(company);
        return this;
    }

    @Step("User sets Street {street} to proper text field.")
    public RegistrationFormPortalPage setStreet(String street) {
        log.info("Set street {}", street);
        tf_addressStreet.sendKeys(street);
        return this;
    }

    @Step("User sets Street {postcode} to proper text field.")
    public RegistrationFormPortalPage setPostCode(String postcode) {
        log.info("Set Postcode {}", postcode);
        tf_addressPostCode.sendKeys(postcode);
        return this;
    }

    @Step("User sets City {city} to proper text field.")
    public RegistrationFormPortalPage setCity(String city) {
        log.info("Set City/Town {}", city);
        tf_addressCity.sendKeys(city);
        return this;
    }

    @Step("Select proper option from 'Country' drop down.")
    public RegistrationFormPortalPage selectCountry(String option) {
        selectDropDownOptionDataDriven(option, dd_addressCountry);
        return this;
    }

    @Step("User sets Email {email} to proper text field.")
    public RegistrationFormPortalPage setEmail(String email) {
        log.info("Set Email {}", email);
        tf_email.sendKeys(email);
        return this;
    }

    @Step("User sets Phone {phone} number to proper text field.")
    public RegistrationFormPortalPage setPhone(String phone) {
        log.info("Set Phone {}", phone);
        tf_phone.sendKeys(phone);
        return this;
    }

    @Step("User sets Phone {mobile} number to proper text field.")
    public RegistrationFormPortalPage setMobile(String mobile) {
        log.info("Set Mobile {}", mobile);
        tf_mobile.sendKeys(mobile);
        return this;
    }

    @Step("User sets VAT {vat}  to proper text field.")
    public RegistrationFormPortalPage setVAT(String vat) {
        log.info("Set VAT {}", vat);
        tf_vat.sendKeys(vat);
        return this;
    }

    @Step("User sets profile {profile}  to proper text field.")
    public RegistrationFormPortalPage setProfile(String profile) throws InterruptedException {
        log.info("Set Profile {}", profile);
        tf_profile.sendKeys(profile);
        //Thread.sleep(3000);
        fluentWaitForElement(list_firstOptionFromProfileList, 20, 2);
        tf_profile.sendKeys(Keys.ARROW_DOWN);
        // Thread.sleep(3000);
        tf_profile.sendKeys(Keys.ENTER);
        return this;
    }

    @Step("User selects two regions UK and Europe ")
    public RegistrationFormPortalPage setRegions() {
        fluentWaitForElement(cb_regionUnitedKingdom, 10, 1).click();
        fluentWaitForElement(cb_regionEurope, 10, 1).click();
        return this;
    }

    @Step("Select regions.")
    public RegistrationFormPortalPage setBusinessSector(String businessSector) {
        log.info("Set Business Sector {}", businessSector);
        tf_businessSector.click();
        tf_businessSector.sendKeys(businessSector);
        return this;
    }

    @Step("Accept Terms And Conditions.")
    public RegistrationFormPortalPage acceptTermsAndConditions() {
        log.info("Accept Terms And Conditions");
        cb_receiveNewsAboutTenders.click();
        cb_termsAndConditions.click();
        return this;
    }

    @Step("Submit registration form.")
    public RegistrationFormPortalPage submitRegistrationForm() {
        log.info("Submit registration form.");
        bt_submitForm.click();
        return this;
    }


}

