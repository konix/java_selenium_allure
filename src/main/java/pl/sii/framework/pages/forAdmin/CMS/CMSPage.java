package pl.sii.framework.pages.forAdmin.CMS;

import io.qameta.allure.Step;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.*;
import org.openqa.selenium.support.FindBy;
import pl.sii.framework.base.component.Page;

import static org.assertj.core.api.Assertions.assertThat;


@Slf4j
public class CMSPage extends Page {

    public CMSPage(WebDriver driver) {
        super(driver);
    }


    @FindBy(xpath = "/html[1]/body[1]/aside[1]/nav[1]/ul[1]/li[3]/a[1]/span[1]")
    WebElement lb_configurtion;

    @FindBy(xpath = "//*[@href='/admin/cmsPage/flushCmsCache']")
    WebElement lb_flushCache;

    @FindBy(xpath = "//div[@class='alert alert-info fade in fade-in-alert']")
    WebElement tm_flushCacheAlert;


    @Step("Flush the CMS Cache (in the CMS section)")
    public CMSPage flushCMSCache(String alert_text) {
        try {
            fluentWaitForElement(lb_configurtion, 10, 1).click();
            fluentWaitForElement(lb_flushCache, 10, 1).click();
            log.info("Check if alert message displayed");
            webDriverWait.until(webDriver -> tm_flushCacheAlert.isDisplayed());
            String text_from_alert = tm_flushCacheAlert.getText();
            log.info("Get alert text", text_from_alert);
            log.info("Check if alert message contain proper text");
            assertThat(text_from_alert).contains(alert_text);
            return this;
        } catch (NotFoundException | StaleElementReferenceException | ElementNotVisibleException e) {
            return this;
        }
    }

}

