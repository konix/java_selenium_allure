package pl.sii.portal;
/*
 * Copyright (c) 2019.  Sii Poland
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import io.qameta.allure.*;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.parallel.Execution;
import org.junit.jupiter.api.parallel.ExecutionMode;
import pl.sii.base.BaseTest;

import static org.assertj.core.api.Assertions.assertThat;


@DisplayName("Logged As Customer")
@Execution(ExecutionMode.SAME_THREAD)
public class LoggedInCustomerTest extends BaseTest {

    @Link("https://page/about/below/tc.html#gid=0&range=A23")
    @Test
    @Tag("portal")
    @Feature("Feature: Logged in customer: add search profile to customer product")
    @Severity(SeverityLevel.CRITICAL)
    @DisplayName("Logged in customer: add search profile to customer product")
    @Description("This TC verify if customer is able to add search profile to customer product")


    public void addSearchProfileToCustomerProduct() {
        assertThat(
                application.openPortal()
                        .loginToThePortal("test@mail.com", "XYZ")
                        .acceptCookies()
                        .openIndustryProfilesPage()
                        .openCustomSearchProfile()
                        .openSaveSearchProfileForm()
                        .setProfileName("Just New Search Profile!")
                        .addContact()
                        .setFirstContactData("Female", "John", "Doe", "testmail@mail.com")
                        .setSecondContactData("Male", "Jim", "Beam", "jim233@o2.pl")
                        .submitForm()
                        .verifyIfSPHasBeenCreated()
                        .navigateToTheMyProductsPage()
                        .verifyIfSPHasBeenCreatedOnMyProductsPage("Just New Search Profile!", "Just New Search Profile! (testmail@mail.com, jim233@o2.pl)")
        );
    }

    @Link("https://page/about/below/tc.html#gid=0&range=A24")
    @Test
    @Tag("portal")
    @Feature("Logged in customer: edit search profile, change some field values, save it and reopen it again")
    @Severity(SeverityLevel.CRITICAL)
    @DisplayName("Logged in customer: edit search profile, change some field values, save it and reopen it again")
    @Description("This TC verify if customer is able to edit search profile, change some field values, save it and reopen it again")


    public void editSearchProfileChangeSomeFieldsSaveItAndReopen() {
        assertThat(
                application.openPortal()
                        .loginToThePortal("test@mail.com", "XYZ")
                        .acceptCookies()
                        .openMyProductsPage()
                        .openEditFormForLatestSearchProfile()
                        .editAndVerifySearchResult("15002900", "Edited Search Profile")
                        .saveEditedSearchProfile()
                        .verifyIfSPHasBeenEdited()
                        .navigateToTheMyProductsPage()
                        .openTheLatestSearchProfile()
                        .verifyTendersResultListFromGlobalVar()
        );
    }

    @Link("https://page/about/below/tc.html#gid=0&range=A25")
    @Test
    @Tag("portal")
    @Feature("Logged in customer: search with a search profile from the customer product")
    @Severity(SeverityLevel.CRITICAL)
    @DisplayName("Logged in customer: search with a search profile from the customer product")
    @Description("This TC verify if customer is able to  search with a search profile from the customer product")


    public void searchWithASearchProfileFromTheCustomerProduct() {
        assertThat(
                application.openPortal()
                        .loginToThePortal("test@mail.com", "XYZ")
                        .acceptCookies()
                        .openMyProductsPage()
                        .openTheLatestSearchProfile()
                        .verifyTendersResultListFromGlobalVar()
        );
    }

    @Link("https://page/about/below/tc.html#gid=0&range=A27")
    @Test
    @Tag("portal")
    @Feature("Logged in customer: delete the search profile")
    @Severity(SeverityLevel.CRITICAL)
    @DisplayName("Logged in customer: delete the search profile")
    @Description("This TC verify if customer is able to delete the search profile")


    public void deleteTheSearchProfile() {
        assertThat(
                application.openPortal()
                        .loginToThePortal("test@mail.com", "XYZ")
                        .acceptCookies()
                        .openMyProductsPage()
                        .deleteTheLatestSearchProfile()
                        .verifyIfSPHasBeenDeleted()
        );
    }
}
