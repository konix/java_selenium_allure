package pl.sii.admin;
/*
 * Copyright (c) 2019.  Sii Poland
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import io.qameta.allure.Description;
import io.qameta.allure.Feature;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.parallel.Execution;
import org.junit.jupiter.api.parallel.ExecutionMode;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;
import pl.sii.base.BaseTest;


import static org.assertj.core.api.Assertions.assertThat;


@DisplayName("Create new tenders")
@Execution(ExecutionMode.CONCURRENT)
public class NewTendersTest extends BaseTest {

    @Tag("admin")
    @Feature("Feature: Create new Tenders")
    @Severity(SeverityLevel.NORMAL)
    @DisplayName("Create new tenders")
    @Description("This TC verify if Admin can create new tender.")
    @ParameterizedTest
    @CsvFileSource(resources = "/TendersDataSet.csv", numLinesToSkip = 1, delimiter = ';')

    public void newTendersTest(String fulltext, String language, String title, String publication_date,
                               String system_expiration_date, String nature_of_contract, String type_of_document,
                               String status, String realisation_address_nuts) {
        assertThat(
                application.open()
                        .signIn()
                        .withEmail("XYZ")
                        .withPassword("XYZ")
                        .submit()
                        .checkIfEmailCodeIsRequired("XYZ")
                        .checkMenuElements()
                        .navigateToDefaultTenderForm()
                        .setTitle(title)
                        .setFullText(fulltext)
                        .setLanguage(language)
                        .setPublicationDate(publication_date)
                        .setSystemExpirationDate(system_expiration_date)
                        .setNatureOfContract(nature_of_contract)
                        .setTypeOfDocument(type_of_document)
                        .setRealisationAddressNutsCode(realisation_address_nuts)
                        .setStatus(status)
        );
    }
}
