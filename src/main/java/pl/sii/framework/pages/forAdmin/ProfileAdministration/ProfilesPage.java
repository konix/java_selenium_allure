package pl.sii.framework.pages.forAdmin.ProfileAdministration;

import io.qameta.allure.Step;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.*;
import org.openqa.selenium.support.FindBy;
import pl.sii.framework.base.component.Page;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;


@Slf4j
public class ProfilesPage extends Page {

    public ProfilesPage(WebDriver driver) {
        super(driver);
    }

    //Left Menu
    @FindBy(xpath = "//*[@href='/admin/searchProfile/list?active=true&searchProfileType=BRANCH&customized=false&token=']")
    WebElement lb_industryProfiles;

    @FindBy(xpath = "//*[@href='/admin/searchProfile/list?active=true&searchProfileType=REGION&customized=false&token=']")
    WebElement lb_regionProfiles;

    @FindBy(xpath = "//*[@href='/admin/searchProfile/list?active=true&searchProfileType=CUSTOMER&customized=false&token=']")
    WebElement lb_userSearchProfiles;

    @FindBy(xpath = "/html[1]/body[1]/div[3]/div[2]/div[1]/div[1]/div[1]")
    WebElement lb_search;

    @FindBy(xpath = "//input[@id='searchString']")
    WebElement tf_textSearch;

    @FindBy(xpath = "//select[@id='fieldsToMatch']")
    WebElement dd_textSearchOptions;

    @FindBy(xpath = "//input[@id='searchTerm']")
    WebElement tf_searchBySearchTerm;

    @FindBy(xpath = "/html[1]/body[1]/div[3]/div[2]/div[1]/div[2]/div[1]/form[1]/fieldset[1]/section[3]/div[1]/div[2]/div[1]/label[1]/i[1]")
    WebElement rb_fieldsCombinationAnd;

    @FindBy(xpath = "//*[@value='OR']")
    WebElement rb_fieldsCombinationOr;

    @FindBy(xpath = "//select[@name='searchContext']")
    WebElement dd_searchContext;

    @FindBy(xpath = "//div[@class='col col-4']//label[@class='checkbox']//i")
    WebElement cb_showDeletedProfiles;

    @FindBy(xpath = "//input[@name='_action_searchForProfileByIndexableFields']")
    WebElement bt_search;


    @FindBy(xpath = "//table[@id='dt_basic']//tr")
    List<WebElement> list_searchResultsRecords;

    //pagination
    @FindBy(xpath = "//a[contains(text(),'»')]")
    WebElement bt_next;


    @Step("Open Industry Profiles List")
    public ProfilesPage openIndustryProfilesList() {
        fluentWaitForElement(lb_industryProfiles, 10, 1).click();
        return this;
    }

    @Step("Open Region Profiles List")
    public ProfilesPage openRegionProfilesList() {
        fluentWaitForElement(lb_regionProfiles, 10, 1).click();
        return this;
    }

    @Step("Open user Search Profiles List")
    public ProfilesPage openUserSearchProfilesList() {
        fluentWaitForElement(lb_userSearchProfiles, 10, 1).click();
        return this;
    }

    @Step("Pagination handling")
    public ProfilesPage handlingPagination() {
        try {
            List<WebElement> namesElements = list_searchResultsRecords;
            List<String> names = new ArrayList<String>();
            for (WebElement namesElement : namesElements) {
                names.add(namesElement.getText());
            }


            while (bt_next.isDisplayed()) {
                fluentWaitForElement(bt_next, 10, 1).click();
                for (WebElement namesElement : namesElements) {
                    names.add(namesElement.getText());
                }
            }

            for (String name : names) {
                log.info(name);
            }


        } catch (NoSuchElementException e) {
            log.info("Login not require to provide Email code");
        }
        return this;
    }

    @Step("Open user Search Profiles List")
    public ProfilesPage searchProfiles(String textSearch, String textSearchOptions, String searchBySearchTerm, String fieldsCombination, String searchContext, String showDeletedProfiles) {
        fluentWaitForElement(lb_search, 10, 1).click();

        if (textSearch != null) {
            tf_textSearch.sendKeys(textSearch);
        }

        if (searchBySearchTerm != null) {
            tf_searchBySearchTerm.sendKeys(searchBySearchTerm);
        }

        if (fieldsCombination.equals("AND")) {
            rb_fieldsCombinationAnd.click();
        }

        if (showDeletedProfiles.equals("yes")) {
            fluentWaitForElement(cb_showDeletedProfiles, 10, 2).click();
        }

        selectDropDownOptionDataDriven(textSearchOptions, dd_textSearchOptions);
        selectDropDownOptionDataDriven(searchContext, dd_searchContext);
        fluentWaitForElement(bt_search, 10, 1).click();
        return this;
    }

}

