package pl.sii.framework.pages.forAdmin.Administration;

import io.qameta.allure.Step;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.*;
import org.openqa.selenium.support.FindBy;
import pl.sii.framework.base.component.Page;

import java.nio.file.*;


import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;


@Slf4j
public class ManagerTemplatePage extends Page {

    public ManagerTemplatePage(WebDriver driver) {
        super(driver);
    }

    @FindBy(xpath = "//input[@id='username']")
    WebElement tf_userName;

    @FindBy(xpath = "//input[@id='password']")
    WebElement tf_userPassword;

    @FindBy(xpath = "//input[@name='shortcut']")
    WebElement tf_shortcut;

    @FindBy(xpath = "//*[@href='/admin/manager/create']")
    WebElement bt_addManager;

    @FindBy(xpath = "//input[@id='academicTitle']")
    WebElement tf_academicTitle;

    @FindBy(xpath = "//input[@id='email']")
    WebElement tf_email;

    @FindBy(xpath = "//input[@id='emailSender']")
    WebElement tf_email_sender;

    @FindBy(xpath = "//input[@id='phone']")
    WebElement tf_phone;

    @FindBy(xpath = "//input[@id='fax']")
    WebElement tf_fax;

    @FindBy(xpath = "//input[@id='lastname']")
    WebElement tf_lastName;

    @FindBy(xpath = "//input[@id='firstname']")
    WebElement tf_firstName;

    @FindBy(xpath = "//textarea[@id='notes']")
    WebElement ta_notes;

    @FindBy(xpath = "//select[@id='gender']")
    WebElement dd_gender;

    @FindBy(xpath = "//select[@id='language.id']")
    WebElement dd_language;

    @FindBy(xpath = "//select[@id='portals']")
    WebElement ms_portals;

    @FindBy(xpath = "//select[@id='authorities']")
    WebElement ms_authorities;

    @FindBy(xpath = "//select[@id='searchContexts']")
    WebElement ms_searchContexts;

    @FindBy(xpath = "//input[@name='picture']")
    WebElement tf_emailPicture;


    @FindBy(xpath = "//div[@class='navbar navbar-default']//li")
    List<WebElement> bar_menu;


    @Step("Select proper gender")
    public ManagerTemplatePage setGender(String option) {
        selectDropDownOptionDataDriven(option, dd_gender);
        return this;
    }

    @Step("Select proper language")
    public ManagerTemplatePage setLanguage(String option) {
        selectDropDownOptionDataDriven(option, dd_language);
        return this;
    }

    @Step("Select proper portal")
    public ManagerTemplatePage setPortals(String option) {
        selectMultipleElementsDataDriven(option, ms_portals);
        return this;
    }

    @Step("Select proper authorities")
    public ManagerTemplatePage setAuthorities(String option) {
        selectMultipleElementsDataDriven(option, ms_authorities);
        return this;
    }

    @Step("Select proper searchcontexts ")
    public ManagerTemplatePage setSearchcontexts(String option) {
        selectMultipleElementsDataDriven(option, ms_searchContexts);
        return this;
    }


    @Step("Check if menu bar is shown")
    public ManagerTemplatePage checkMenuElements() {
        checkElementsAreDisplayed(bar_menu, 5);
        return this;
    }

    @Step("User sets login {userName} to login input field")
    public ManagerTemplatePage setUserName(String userName) {
        log.info("Set username {}", userName);
        tf_userName.sendKeys(userName);
        return this;
    }

    @Step("User sets academic title {academicTitle} to proper input field")
    public ManagerTemplatePage setAcademicTitle(String academicTitle) {
        log.info("Set academic title {}", academicTitle);
        tf_academicTitle.sendKeys(academicTitle);
        return this;
    }

    @Step("User sets email {email} to proper input field")
    public ManagerTemplatePage setManagerEmail(String email) {
        log.info("Set manager email {}", email);
        tf_email.sendKeys(email);
        return this;
    }

    @Step("User sets email {emailSender} to proper input field")
    public ManagerTemplatePage setEmailSender(String emailSender) {
        log.info("Set manager email {}", emailSender);
        tf_email_sender.sendKeys(emailSender);
        return this;
    }

    @Step("User sets firstname {firstName} to proper input field")
    public ManagerTemplatePage setFirstName(String firstName) {
        log.info("Set firstname  {}", firstName);
        tf_firstName.sendKeys(firstName);
        return this;
    }


    @Step("User sets lastname {lastName} to proper input field")
    public ManagerTemplatePage setLastName(String lastName) {
        log.info("Set lastname  {}", lastName);
        tf_lastName.sendKeys(lastName);
        return this;
    }

    @Step("User sets shortcut {fax} to proper input field")
    public ManagerTemplatePage setFax(String fax) {
        log.info("Set academic title {}", fax);
        tf_fax.sendKeys(fax);
        return this;
    }

    @Step("User sets phone {phone} to proper input field")
    public ManagerTemplatePage setPhone(String phone) {
        log.info("Set phone {}", phone);
        tf_phone.sendKeys(phone);
        return this;
    }

    @Step("User sets note {notes} to proper input field")
    public ManagerTemplatePage setNotes(String notes) {
        log.info("Set note  {}", notes);
        ta_notes.sendKeys(notes);
        return this;
    }

    @Step("User sets shortcut {shortcut} to proper input field")
    public ManagerTemplatePage setShortcut(String shortcut) {
        log.info("Set academic title {}", shortcut);
        tf_shortcut.sendKeys(shortcut);
        return this;
    }

    @Step("User set login {password} to login input field")
    public ManagerTemplatePage setUserPassword(String password) {
        log.info("Set username {}", password);
        tf_userPassword.sendKeys(password);
        return this;
    }

    @Step("User clicks on Add Manager button")
    public ManagerTemplatePage openAddManagerForm() {
        log.info("Click on Add Manager button.");
        bt_addManager.click();
        return this;
    }

    @Step("User upload a file {fileName} to proper input field")
    public ManagerTemplatePage uploadFile(String fileName) {
        log.info("Upload a file  {}", fileName);
        String path = Paths.get(".").toAbsolutePath().normalize().toString();
        log.info("Path {}", path);
        tf_emailPicture.sendKeys(path + "/src/test/resources/" + fileName);

        return this;
    }
}