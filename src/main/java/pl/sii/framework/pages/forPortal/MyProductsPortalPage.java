package pl.sii.framework.pages.forPortal;

import io.qameta.allure.Step;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import pl.sii.framework.base.component.Page;
import pl.sii.framework.pages.forAdmin.AdminPage;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;


@Slf4j
public class MyProductsPortalPage extends Page {

    public MyProductsPortalPage(WebDriver driver) {
        super(driver);
    }

    //Custom Search

    @FindBy(xpath = "//*[@href='/private/custom/searchprofile/index']")
    WebElement lb_customSearch;

    @FindBy(xpath = "//div[@class='publicMessage']")
    WebElement lb_publicMessage;


    //MENU
    @FindBy(xpath = "//tr[@class='even' or @class='odd']")
    List<WebElement> allAvailableSearchProfiles;

    @FindBy(xpath = "/html[1]/body[1]/div[9]/div[3]/div[2]/div[2]/table[1]/tbody[1]/tr[2]/td[4]/div[1]/a[1]")
    WebElement lb_latestPencilEdit;

    @FindBy(xpath = "/html[1]/body[1]/div[9]/div[3]/div[2]/div[2]/table[1]/tbody[1]/tr[2]/td[5]/div[1]/a[1]")
    WebElement lb_magnifierSearchOldest;

    @FindBy(xpath = "/html[1]/body[1]/div[9]/div[3]/div[2]/div[2]/table[1]/tbody[1]/tr[2]/td[6]/div[1]/a[1]")
    WebElement lb_xDeleteOldest;

    @FindBy(xpath = "/html[1]/body[1]/div[9]/div[3]/div[2]/div[2]/table[1]/tbody[1]/tr[2]/td[1]")
    WebElement lb_latestSearchProfileName;

    @FindBy(xpath = "/html[1]/body[1]/div[9]/div[3]/div[2]/div[2]/table[1]/tbody[1]/tr[2]/td[2]/div[1]/div[1]/a[1]")
    WebElement lb_latestSearchProfileRecipients;


    @FindBy(xpath = "//ul[@id='first-nav']//a[@class='link1'][contains(text(),'Products')]")
    WebElement lb_products;

    @FindBy(xpath = "//*[@id='cookies']")
    WebElement bt_acceptCookies;


    @Step("Verify if new SP has been successfully created ")
    public MyProductsPortalPage verifyIfSPHasBeenCreatedOnMyProductsPage(String nameCell, String recipientsCell) {
        if (allAvailableSearchProfiles.size() > 0) {
            lb_latestSearchProfileName.isDisplayed();
            lb_latestSearchProfileRecipients.isDisplayed();
            customCheckText(nameCell, lb_latestSearchProfileName);
            customCheckText(recipientsCell, lb_latestSearchProfileRecipients);
        } else {
            assertThat(allAvailableSearchProfiles.size()).isGreaterThan(0);
        }
        return this;
    }

    @Step("Open edit form for latest search profile.")
    public PrivateSearchProfilePortalPage openEditFormForLatestSearchProfile() {
        log.info("Click on the edit icon of latest search profile");
        lb_latestPencilEdit.click();
        return pageFactory.create(PrivateSearchProfilePortalPage.class);
    }

    @Step("Delete the latest search profile")
    public MyProductsPortalPage deleteTheLatestSearchProfile() {
        log.info("Delete the latest search profile");
        lb_xDeleteOldest.click();
        confirmAlertMessage();
        return this;
    }

    @Step("Verify if new SP has been successfully deleted.")
    public MyProductsPortalPage verifyIfSPHasBeenDeleted() {
        log.info("Verify if new SP has been successfully deleted.");
        lb_publicMessage.isDisplayed();
        customCheckText("Search profile Edited Search Profile successful deleted", lb_publicMessage);

        return this;
    }


    @Step("Click on the search icon of oldest search profile")
    public SearchResultPortalPage openTheLatestSearchProfile() {
        log.info("Click on the search icon if oldest search profile");
        fluentWaitForElement(lb_magnifierSearchOldest, 10, 1).click();
        return pageFactory.create(SearchResultPortalPage.class);
    }


}

