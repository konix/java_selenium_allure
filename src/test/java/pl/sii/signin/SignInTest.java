/*
 * Copyright (c) 2019.  Sii Poland
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.sii.signin;

import io.qameta.allure.*;
import net.bytebuddy.utility.RandomString;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.parallel.Execution;
import org.junit.jupiter.api.parallel.ExecutionMode;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;
import pl.sii.base.BaseTest;

import static org.assertj.core.api.Assertions.*;


@DisplayName("Login with incorrect login and password ")
@Execution(ExecutionMode.CONCURRENT)
public class SignInTest extends BaseTest {

    @Test
    @Tag("signin")
    @Feature("Feature: Sign in and Sign out")
    @Severity(SeverityLevel.CRITICAL)
    @DisplayName("Login with incorrect login and password ")
    @Description("This TC verify if user cannot login as a admin with incorrect login and password")
    public void errorMessageShouldBeVisibleWhenIncorrectCredentialsProvided() {
        assertThat(
                application.open()
                        .signIn()
                        .checkTitle("XYZ: Login")
                        .withEmail("user_wrong")
                        .withPassword("password_wrong")
                        .submit()
                        .checkTitle("XYZ: Login")
                        .isAlertMessageDisplayed("Login failed. Incorrect user name or password.")
        );

    }

    @Link("https://page/about/below/tc.html#gid=0&range=A4")
    @Link("https://page/about/below/tc.html#gid=0&range=A6")
    @Tag("signin")
    @Feature("Feature: Sign in and Sign out")
    @Severity(SeverityLevel.CRITICAL)
    @DisplayName("Sign in and Sign out to application with correct credentials")
    @Description("This TC verify if admin can login and logout from the system")
    @ParameterizedTest
    @CsvFileSource(resources = "/CredentialsDataSet.csv", numLinesToSkip = 1, delimiter = ';')
    public void adminPageShouldBeVisibleWhenCorrectCredentialsProvided(String username, String password) {
        assertThat(
                application.open()
                        .signIn()
                        .checkTitle("XYZ: Login")
                        .withEmail(username)
                        .withPassword(password)
                        .submit()
                        .checkIfEmailCodeIsRequired(username)
                        .checkTitle("XYZ: Tender Admin")
                        .navigateToAdministrationPage()
                        .checkIfLoggedUserNameIsCorrect(username)
                        .logout()
                        .checkTitle("XYZ: Login")


        );
    }
}